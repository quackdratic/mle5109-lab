package com.benchmarks;

import models.Order;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OrderFactory {
    public static int randomInt(int greaterOrEq, int lessOrEq) {
        return (int) ((Math.random() * (lessOrEq - greaterOrEq)) + greaterOrEq);
    }

    public static Order randomOrder() {
        return new Order(
                randomInt(1, 2000000000),
                randomInt(50, 5000),
                randomInt(1, 100)
        );
    }

    public static Order randomOrder(Set<Integer> excludedIds) {
        Order order = null;
        boolean find = true;
        while (find) {
            order = randomOrder();
            int id = order.getId();
            find = excludedIds.contains(id);
        }
        return order;
    }

    public static List<Order> orderListOf(int size) {
        List <Order> list = new ArrayList<>();
        Set<Integer> ids = new HashSet<>();
        for (int i=0; i<size; ++i) {
            Order order = randomOrder(ids);
            list.add(order);
            ids.add(order.getId());
        }
        return list;
    }
}
