package com.benchmarks;

import com.repos.InMemoryRepository;
import models.Order;
import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.List;

@State(Scope.Benchmark)
public class RepoAddBenchmark {
    @State(Scope.Thread)
    public static class ExecutionPlan {
        @Param({ "array", "hashset", "treeset", "conchashmap", "treemap", "bimap", "objlist" })
        public String repoType;

        //  10^2, 10^4, 10^5
        @Param({ "100", "10000", "100000"})
        public int intendedSize;

        public InMemoryRepository<Order> repo = null;

        public List<Order> seedArray = new ArrayList<>();

        @Setup(Level.Invocation)
        public void setUp() {
            repo = RepoFactory.strToRepo(repoType);
            seedArray = OrderFactory.orderListOf(intendedSize);
        }
    }

    @Fork(value = 5, warmups = 1)
    @Benchmark
    @BenchmarkMode(Mode.SingleShotTime)
    public void benchAdd(ExecutionPlan plan) {
        for (int i=0; i<plan.intendedSize; i++) {
            plan.repo.add(plan.seedArray.get(i));
        }
    }
}
