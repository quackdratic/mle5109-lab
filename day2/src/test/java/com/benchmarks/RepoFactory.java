package com.benchmarks;

import com.repos.*;
import models.Order;

public class RepoFactory {
    public static InMemoryRepository<Order> strToRepo(String repoType) {
        InMemoryRepository<Order> repo = null;
        if (repoType.equals("array")) {
            repo = new ArrayListBasedRepository<>();
        }   else
        if (repoType.equals("hashset")) {
            repo = new HashSetBasedRepository<>();
        }   else
        if (repoType.equals("treeset")) {
            repo = new TreeSetBasedRepository<>();
        }   else
        if (repoType.equals("conchashmap")) {
            repo = new ConcHashMBasedRepository<>();
        }   else
        if (repoType.equals("treemap")) {
            repo = new TreeMapBasedRepository<>();
        }   else
        if (repoType.equals("bimap")) {
            repo = new BiMapBasedRepository<>();
        }   else
        if (repoType.equals("objlist")) {
            repo = new ObjectArrayListBasedRepository<>();
        }
        return repo;
    }
}
