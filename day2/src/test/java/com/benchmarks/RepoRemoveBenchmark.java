package com.benchmarks;

import com.repos.InMemoryRepository;
import models.Order;
import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.List;

public class RepoRemoveBenchmark {
    @State(Scope.Thread)
    public static class ExecutionPlan {
        @Param({ "array", "hashset", "treeset", "conchashmap", "treemap", "bimap", "objlist" })
        public String repoType;

        //  10^2, 10^4, 10^6
        @Param({ "100", "10000", "100000"})
        public int intendedSize;

        public InMemoryRepository<Order> repo = null;

        public List<Order> seedArray = new ArrayList<>();

        @Setup(Level.Invocation)
        public void setUp() {
            repo = RepoFactory.strToRepo(repoType);
            seedArray = OrderFactory.orderListOf(intendedSize);
            for (int i=0; i<intendedSize; i++) {
                repo.add(seedArray.get(i));
            }
        }
    }

    @Fork(value = 5, warmups = 1)
    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    public void benchRemove(RepoContainsBenchmark.ExecutionPlan plan) {
        for (int i=0; i<plan.intendedSize; i++) {
            plan.repo.remove(plan.seedArray.get(i));
        }
    }
}
