package com.repos;

import java.util.TreeSet;
import java.util.Set;

public class TreeSetBasedRepository <T extends Comparable<T>> implements InMemoryRepository <T>{
    private final Set<T> set = new TreeSet<>();

    public TreeSetBasedRepository() {}

    @Override
    public void add(T toAdd) {
        set.add(toAdd);
    }

    @Override
    public boolean contains(T toAdd) {
        return set.contains(toAdd);
    }

    @Override
    public void remove(T toRemove) {
        set.remove(toRemove);
    }
}
