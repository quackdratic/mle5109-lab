package com.repos;

import org.eclipse.collections.api.bimap.MutableBiMap;
import org.eclipse.collections.api.map.MapIterable;
import org.eclipse.collections.impl.bimap.mutable.HashBiMap;

public class BiMapBasedRepository <T> implements InMemoryRepository <T>{
    private final MutableBiMap<Integer, T> map = new HashBiMap<>();

    public BiMapBasedRepository() {}

    @Override
    public void add(T toAdd) {
        map.put(toAdd.hashCode(), toAdd);
    }

    @Override
    public boolean contains(T toAdd) {
        return map.containsKey(toAdd.hashCode());
    }

    @Override
    public void remove(T toRemove) {
        map.remove(toRemove.hashCode());
    }
}
