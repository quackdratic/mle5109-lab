package com.repos;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository <T> implements InMemoryRepository <T>{
    private final Set<T> set = new HashSet<>();

    public HashSetBasedRepository() {}

    @Override
    public void add(T toAdd) {
        set.add(toAdd);
    }

    @Override
    public boolean contains(T toAdd) {
        return set.contains(toAdd);
    }

    @Override
    public void remove(T toRemove) {
        set.remove(toRemove);
    }
}
