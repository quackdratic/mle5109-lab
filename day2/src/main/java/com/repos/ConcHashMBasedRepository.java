package com.repos;

import java.util.AbstractMap;
import java.util.concurrent.ConcurrentHashMap;

public class ConcHashMBasedRepository <T> implements InMemoryRepository <T>{
    private final AbstractMap<Integer, T> map = new ConcurrentHashMap<>();

    public ConcHashMBasedRepository() {}

    @Override
    public void add(T toAdd) {
        map.put(toAdd.hashCode(), toAdd);
    }

    @Override
    public boolean contains(T toAdd) {
        return map.containsKey(toAdd.hashCode());
    }

    @Override
    public void remove(T toRemove) {
        map.remove(toRemove.hashCode());
    }
}
