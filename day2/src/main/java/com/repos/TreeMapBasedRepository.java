package com.repos;

import java.util.Map;
import java.util.TreeMap;

public class TreeMapBasedRepository <T extends Comparable<T>> implements InMemoryRepository <T>{
    private final Map<Integer, T> map = new TreeMap<>();

    public TreeMapBasedRepository() {}

    @Override
    public void add(T toAdd) {
        map.put(toAdd.hashCode(), toAdd);
    }

    @Override
    public boolean contains(T toAdd) {
        return map.containsKey(toAdd.hashCode());
    }

    @Override
    public void remove(T toRemove) {
        map.remove(toRemove.hashCode());
    }
}
