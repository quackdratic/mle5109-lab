package com.repos;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;

public class ObjectArrayListBasedRepository <T> implements InMemoryRepository <T>{
    private final List<T> list = new ObjectArrayList<>();

    public ObjectArrayListBasedRepository() {}

    @Override
    public void add(T toAdd) {
        list.add(toAdd);
    }

    @Override
    public boolean contains(T toAdd) {
        return list.contains(toAdd);
    }

    @Override
    public void remove(T toRemove) {
        list.remove(toRemove);
    }
}
