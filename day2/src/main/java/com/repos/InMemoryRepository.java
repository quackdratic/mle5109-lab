package com.repos;

public interface InMemoryRepository <T> {
    void add(T toAdd);
    boolean contains(T toAdd);
    void remove(T toRemove);
}
