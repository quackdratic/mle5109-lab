package com.repos;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository <T> implements InMemoryRepository <T>{
    private final List<T> list = new ArrayList<>();

    public ArrayListBasedRepository() {}

    @Override
    public void add(T toAdd) {
        list.add(toAdd);
    }

    @Override
    public boolean contains(T toAdd) {
        return list.contains(toAdd);
    }

    @Override
    public void remove(T toRemove) {
        list.remove(toRemove);
    }
}
