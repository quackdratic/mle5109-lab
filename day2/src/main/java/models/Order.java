package models;

public class Order implements Comparable<Order> {
    private final int id;
    private final int price;
    private final int quantity;

    public Order(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Order) {
            Order cast = (Order) other;
            return id == cast.id;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    @Override
    public String toString() {
        return "{" +
                "id: " + this.id + "," +
                "price: " + this.price + "," +
                "quantity: " + this.quantity +
                "}";
    }

    @Override
    public int compareTo(Order other) {
        if (other == null) return -1;
        return Integer.compare(other.price, this.price);
    }

    public int getId() {
        return id;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }
}
