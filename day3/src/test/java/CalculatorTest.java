import com.data.BigDecimalsProvider;
import com.data.Dataset;
import com.service.BigDecimalCalculator;
import com.service.BigDecimalSerializer;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CalculatorTest {
    private final BigDecimalCalculator service = new BigDecimalCalculator(5);

    public void testSum(Dataset dataset) {
        assertEquals(dataset.getSum().toString(),
                service.sumOf(dataset.getNumbers()).toString());
    }

    public void testAvg(Dataset dataset) {
        assertTrue(dataset.getAvg()
                .min(service.avgOf(dataset.getNumbers()))
                .abs()
                .compareTo(new BigDecimal("0.01")) > 0);
    }

    public void testTop10(Dataset dataset) {
        List<BigDecimal> top10 = new ArrayList<>(dataset.getTop10());
        List<BigDecimal> res = service.top10prc(dataset.getNumbers());
        top10.sort(BigDecimal::compareTo);
        res.sort(BigDecimal::compareTo);

        for (int idx=0; idx<top10.size(); idx++) {
            assertEquals(top10.get(idx).toString(),
                    res.get(idx).toString());
        }
    }

    @Test
    public void testSumAll() {
        testSum(BigDecimalsProvider.getBigDecimalsSmall());
        testSum(BigDecimalsProvider.getBigDecimalsMedium());
        testSum(BigDecimalsProvider.getBigDecimalsLarge());
        testSum(BigDecimalsProvider.getBigDecimalsHuge());
    }

    @Test
    public void testAvgAll() {
        testAvg(BigDecimalsProvider.getBigDecimalsSmall());
        testAvg(BigDecimalsProvider.getBigDecimalsMedium());
        testAvg(BigDecimalsProvider.getBigDecimalsLarge());
        testAvg(BigDecimalsProvider.getBigDecimalsHuge());
    }

    @Test
    public void testTopAll() {
        testTop10(BigDecimalsProvider.getBigDecimalsSmall());
        testTop10(BigDecimalsProvider.getBigDecimalsMedium());
        testTop10(BigDecimalsProvider.getBigDecimalsLarge());
        testTop10(BigDecimalsProvider.getBigDecimalsHuge());
    }

    @Test
    public void testSerialize() {
        try {
            Dataset dataset = BigDecimalSerializer.deserializeDataset("C:/Users/DTodasca/Documents/decimals.txt");
            assertEquals(dataset.getNumbers().size(), BigDecimalsProvider.HUGE_SIZE);
            testSum(dataset);
            testAvg(dataset);
            testTop10(dataset);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
