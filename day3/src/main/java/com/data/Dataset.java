package com.data;

import java.math.BigDecimal;
import java.util.List;

public class Dataset {
    private final List<BigDecimal> numbers;
    private final BigDecimal sum;
    private final BigDecimal avg;
    private final List<BigDecimal> top10;

    public Dataset(List<BigDecimal> numbers, BigDecimal sum, BigDecimal avg, List<BigDecimal> top10) {
        this.numbers = numbers;
        this.sum = sum;
        this.avg = avg;
        this.top10 = top10;
    }

    public List<BigDecimal> getNumbers() {
        return numbers;
    }

    public List<BigDecimal> getTop10() {
        return top10;
    }

    public BigDecimal getAvg() {
        return avg;
    }

    public BigDecimal getSum() {
        return sum;
    }
}
