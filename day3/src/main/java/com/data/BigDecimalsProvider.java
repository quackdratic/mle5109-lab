package com.data;

import javax.xml.crypto.Data;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class BigDecimalsProvider {
    public static int HUGE_SIZE = 1000;

    public static Dataset getBigDecimalsSmall() {
        return new Dataset(
                List.of(new BigDecimal("10")),
                new BigDecimal("10"),
                new BigDecimal("10"),
                List.of()
        );
    }

    public static Dataset getBigDecimalsMedium() {
        return new Dataset(
                List.of(new BigDecimal("10"), new BigDecimal("1"),
                        new BigDecimal("100"), new BigDecimal("1000")),
                new BigDecimal("1111"),
                new BigDecimal("277.75"),
                List.of()
        );
    }

    public static Dataset getBigDecimalsLarge() {
        return new Dataset(
                List.of(new BigDecimal("1"), new BigDecimal("1"), new BigDecimal("1"),
                        new BigDecimal("1"), new BigDecimal("1"), new BigDecimal("1"),
                        new BigDecimal("1"), new BigDecimal("1"), new BigDecimal("1"),
                        new BigDecimal("1")),
                new BigDecimal("10"),
                new BigDecimal("1"),
                List.of(new BigDecimal("1"))
        );
    }

    public static Dataset getBigDecimalsHuge() {
        return getBigDecimalsHuge(HUGE_SIZE, 5);
    }

    public static Dataset getBigDecimalsHuge(int hugeSize, int factor) {
        List <BigDecimal> decimals = new ArrayList<>();
        List <BigDecimal> top10 = new ArrayList<>();

        for (int idx=0; idx<hugeSize; idx++) {
            BigDecimal num = new BigDecimal((idx + 1) * factor);
            decimals.add(num);
            if (idx >= 9*(hugeSize/10)) {
                top10.add(num);
            }
        }

        BigDecimal sum = new BigDecimal(hugeSize)
                .multiply(new BigDecimal(hugeSize+1))
                .multiply(new BigDecimal(factor))
                .divide(new BigDecimal(2), 0, RoundingMode.DOWN);
        BigDecimal avg = sum.divide(new BigDecimal(hugeSize), 5, RoundingMode.DOWN);

        return new Dataset(decimals, sum, avg, top10);
    }
}
