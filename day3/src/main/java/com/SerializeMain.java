package com;

import com.data.BigDecimalsProvider;
import com.service.BigDecimalSerializer;

public class SerializeMain {
    public static void main(String[] args) {
        try {
            BigDecimalSerializer.serializeDataset(
                    BigDecimalsProvider.getBigDecimalsHuge(),
                    "C:/Users/DTodasca/Documents/decimals.txt"
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
