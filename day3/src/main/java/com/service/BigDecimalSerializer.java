package com.service;

import com.data.Dataset;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BigDecimalSerializer {
    public static void serializeDataset(Dataset dataset, String path)
            throws Exception {
        FileOutputStream fileStream = new FileOutputStream(path);
        ObjectOutputStream objectStream = new ObjectOutputStream(fileStream);
        objectStream.writeInt(dataset.getNumbers().size());
        dataset.getNumbers().forEach(number -> {
            try {
                objectStream.writeObject(number);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        objectStream.writeObject(dataset.getSum());
        objectStream.writeObject(dataset.getAvg());
        objectStream.writeInt(dataset.getTop10().size());
        dataset.getTop10().forEach(number -> {
            try {
                objectStream.writeObject(number);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        objectStream.flush();
        objectStream.close();
    }

    public static Dataset deserializeDataset(String path)
            throws Exception {
        FileInputStream fileStream = new FileInputStream (path);
        ObjectInputStream objectStream = new ObjectInputStream (fileStream);

        int numSize = objectStream.readInt();
        List<BigDecimal> numbers = new ArrayList<>();
        for (int idx=0; idx<numSize; idx++) {
            numbers.add((BigDecimal) objectStream.readObject());
        }

        BigDecimal sum = (BigDecimal) objectStream.readObject();
        BigDecimal avg = (BigDecimal) objectStream.readObject();

        int topSize = objectStream.read();
        List<BigDecimal> top10 = new ArrayList<>();
        for (int idx=0; idx<topSize; idx++) {
            top10.add((BigDecimal) objectStream.readObject());
        }

        return new Dataset(numbers, sum, avg, top10);
    }
}

