package com.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BigDecimalCalculator {
    private final int floatPrecision;
    public BigDecimalCalculator(int floatPrecision) {
        this.floatPrecision = floatPrecision;
    }

    public BigDecimal sumOf(List<BigDecimal> nums) {
        return nums.stream().reduce(new BigDecimal("0"), BigDecimal::add);
    }

    public BigDecimal avgOf(List<BigDecimal> nums) {
        BigDecimal weight = new BigDecimal(nums.size());
        return sumOf(nums).divide(weight, this.floatPrecision, RoundingMode.DOWN);
    }

    public List<BigDecimal> top10prc(List<BigDecimal> nums) {
        int top10size = nums.size() / 10;
        return nums.stream().sorted(Comparator.reverseOrder()).limit(top10size).collect(Collectors.toList());
    }
}
