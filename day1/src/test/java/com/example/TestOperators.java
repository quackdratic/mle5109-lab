package com.example;

import com.calc.exceptions.MinusException;
import com.calc.exceptions.ZeroException;
import com.calc.numbers.IntegerNumber;
import com.calc.numbers.PairParam;
import com.calc.operators.*;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertThrows;

public class TestOperators {
    IntegerNumber i1;
    IntegerNumber i2;
    IntegerNumber zero;

    @Before
    public void setup() {
        i1 = new IntegerNumber(5);
        i2 = new IntegerNumber(-5);
        zero = new IntegerNumber(0);
    }

    @Test
    public void testPlus() {
        assertThat(new PlusOperator().compute(new PairParam(i1, i2)).num(), equalTo(0));
    }

    @Test
    public void testMinus() {
        assertThat(new MinusOperator().compute(new PairParam(i1, i2)).num(), equalTo(10));
    }

    @Test
    public void testMultiply() {
        assertThat(new MultiplyOperator().compute(new PairParam(i1, i2)).num(), equalTo(-25));
    }

    @Test
    public void testDivide() {
        assertThat(new DivideOperator().compute(new PairParam(i1, i2)).num(), equalTo(-1));
        assertThrows(ZeroException.class, () -> new DivideOperator().compute(new PairParam(i1, zero)));
    }

    @Test
    public void testMin() {
        assertThat(new MinOperator().compute(new PairParam(i1, i2)).num(), equalTo(-5));
    }

    @Test
    public void testMax() {
        assertThat(new MaxOperator().compute(new PairParam(i1, i2)).num(), equalTo(5));
    }

    @Test
    public void testSqrt() {
        assertThat(new SqrtOperator().compute(i1).num(), equalTo(2));
        assertThrows(ZeroException.class, () -> new SqrtOperator().compute(zero));
        assertThrows(MinusException.class, () -> new SqrtOperator().compute(i2));
    }
}
