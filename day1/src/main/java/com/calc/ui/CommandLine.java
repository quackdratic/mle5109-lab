package com.calc.ui;

import com.calc.numbers.IntegerNumber;

import com.calc.numbers.PairParam;
import com.calc.operators.*;

import java.util.Scanner;

public class CommandLine {
    private final Scanner scanner = new Scanner(System.in);
    private IntegerNumber rem = new IntegerNumber(0);

    public void run() {
        while (true) {
            printInfo();
            printMenu();
            int opt = readInt();
            try {
                switch (opt) {
                    case 0:
                        resetValue();
                        break;
                    case 1:
                        plusOperator();
                        break;
                    case 2:
                        minusOperator();
                        break;
                    case 3:
                        multiplyOperator();
                        break;
                    case 4:
                        divideOperator();
                        break;
                    case 5:
                        maxOperator();
                        break;
                    case 6:
                        minOperator();
                        break;
                    case 7:
                        sqrtOperator();
                        break;
                    default:
                        unknownOperator();
                        break;
                }
            }
            catch (Exception err) {
                System.out.println(err.getMessage());
            }

            System.out.println();
        }
    }

    private void printMenu() {
        System.out.println("0. Reset value");
        System.out.println("1. +");
        System.out.println("2. -");
        System.out.println("3. *");
        System.out.println("4. /");
        System.out.println("5. max");
        System.out.println("6. min");
        System.out.println("7. sqrt");
    }

    private void printInfo() {
        System.out.println("Number: " + rem.num());
    }

    private int readInt() {
        return scanner.nextInt();
    }

    private void plusOperator() {
        System.out.println("Operator +");
        int x = readInt();
        IntegerNumber other = new IntegerNumber(x);
        rem = new PlusOperator().compute(new PairParam(rem, other));
    }

    private void minusOperator() {
        System.out.println("Operator -");
        int x = readInt();
        IntegerNumber other = new IntegerNumber(x);
        rem = new MinusOperator().compute(new PairParam(rem, other));
    }

    private void divideOperator() {
        System.out.println("Operator /");
        int x = readInt();
        IntegerNumber other = new IntegerNumber(x);
        rem = new DivideOperator().compute(new PairParam(rem, other));
    }

    private void multiplyOperator() {
        System.out.println("Operator *");
        int x = readInt();
        IntegerNumber other = new IntegerNumber(x);
        rem = new MultiplyOperator().compute(new PairParam(rem, other));
    }

    private void maxOperator() {
        System.out.println("Function max");
        int x = readInt();
        IntegerNumber other = new IntegerNumber(x);
        rem = new MaxOperator().compute(new PairParam(rem, other));
    }

    private void minOperator() {
        System.out.println("Function min");
        int x = readInt();
        IntegerNumber other = new IntegerNumber(x);
        rem = new MinOperator().compute(new PairParam(rem, other));
    }

    private void sqrtOperator() {
        System.out.println("Computing square root");
        rem = new SqrtOperator().compute(rem);
    }

    private void resetValue() {
        System.out.println("Input new value:");
        int x = readInt();
        rem = new IntegerNumber(x);
    }

    private void unknownOperator() {
        System.out.println("Unknown option.");
    }
}
