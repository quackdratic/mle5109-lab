package com.calc;

import com.calc.ui.CommandLine;

public class MainCLI {
    public static void main(String[] args) {
        CommandLine cli = new CommandLine();
        cli.run();
    }
}
