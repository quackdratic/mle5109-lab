package com.calc.operators;

import com.calc.numbers.IntegerNumber;
import com.calc.numbers.PairParam;

public class MinusOperator implements IOperator <IntegerNumber, PairParam> {
    public IntegerNumber compute(PairParam param) {
        return new IntegerNumber(param.getFirst().num() - param.getSecond().num());
    }
}
