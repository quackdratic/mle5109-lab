package com.calc.operators;

public interface IOperator <T1, T2> {
    T1 compute(T2 param);
}
