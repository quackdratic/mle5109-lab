package com.calc.operators;

import com.calc.exceptions.MinusException;
import com.calc.exceptions.ZeroException;
import com.calc.numbers.IntegerNumber;

public class SqrtOperator implements IOperator<IntegerNumber, IntegerNumber> {
    public IntegerNumber compute(IntegerNumber param) {
        if (param.num() == 0) {
            throw new ZeroException("Cannot compute sqrt of zero!");
        }
        if (param.num() < 0) {
            throw new MinusException("Cannot compute sqrt of lesser than zero!");
        }
        int value = (int) Math.sqrt(param.num());
        return new IntegerNumber(value);
    }
}
