package com.calc.operators;

import com.calc.numbers.IntegerNumber;
import com.calc.numbers.PairParam;

public class MaxOperator implements IOperator<IntegerNumber, PairParam> {
    public IntegerNumber compute(PairParam param) {
        int value = Math.max(param.getFirst().num(), param.getSecond().num());
        return new IntegerNumber(value);
    }
}
