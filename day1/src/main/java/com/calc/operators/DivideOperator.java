package com.calc.operators;

import com.calc.exceptions.ZeroException;
import com.calc.numbers.IntegerNumber;
import com.calc.numbers.PairParam;

public class DivideOperator implements IOperator <IntegerNumber, PairParam> {
    public IntegerNumber compute(PairParam param) {
        if (param.getSecond().toInt().num() == 0) {
            throw new ZeroException("Cannot divide by zero!");
        }
        return new IntegerNumber(param.getFirst().num() / param.getSecond().num());
    }
}
