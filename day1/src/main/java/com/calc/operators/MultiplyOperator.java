package com.calc.operators;

import com.calc.numbers.IntegerNumber;
import com.calc.numbers.PairParam;

public class MultiplyOperator implements IOperator <IntegerNumber, PairParam> {
    public IntegerNumber compute(PairParam param) {
        return new IntegerNumber(param.getFirst().num() * param.getSecond().num());
    }
}
