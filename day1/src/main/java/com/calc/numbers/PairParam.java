package com.calc.numbers;

public class PairParam implements IParam {
    private final IntegerNumber first;
    private final IntegerNumber second;

    public PairParam(IntegerNumber first, IntegerNumber second)
    {
        this.first = first;
        this.second = second;
    }

    public IntegerNumber getFirst() { return first; }
    public IntegerNumber getSecond() { return second; }
}
