package com.calc.numbers;

public class IntegerNumber implements IParam {
    public IntegerNumber toInt() {
        return this;
    }
    private final int value;
    public int num() { return value; }
    public IntegerNumber(int value) {
        this.value = value;
    }
}
