package com.calc.exceptions;

public class ZeroException extends RuntimeException {
    public ZeroException(String msg) {
        super(msg);
    }
}
