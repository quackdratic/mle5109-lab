package com.calc.exceptions;

public class MinusException extends RuntimeException {
    public MinusException(String msg) {
        super(msg);
    }
}
