package server;

public interface IServerService {
    void openServer();
    void close();
}
