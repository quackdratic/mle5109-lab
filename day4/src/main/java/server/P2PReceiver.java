package server;

import shared.domain.IMessage;
import shared.domain.TextMessage;
import shared.exceptions.AcceptClientException;
import shared.exceptions.OpenServerException;
import shared.exceptions.ReceiveException;
import shared.repo.SocketRepo;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class P2PReceiver implements IServerService {
    private final ExecutorService executor = Executors.newFixedThreadPool(5);
    private final SocketRepo repo;
    private final int peerPort;
    private ServerSocket acceptSocket = null;

    public P2PReceiver(SocketRepo repo, int port) {
        this.repo = repo;
        this.peerPort = port;
    }

    public void openServer() {
        try {
            acceptSocket = new ServerSocket(peerPort);
            Thread acceptThread = new Thread(this::acceptMultipleClients);
            executor.submit(acceptThread);
        }
        catch (IOException exec) {
            throw new OpenServerException(exec.getMessage());
        }
    }

    private void acceptMultipleClients() {
        while (true) {
            try {
                Socket listeningSocket = acceptSocket.accept();
                repo.addSocket(listeningSocket);
                System.out.println("LOG: new connection from <" + listeningSocket.getInetAddress() + ">!");
            }
            catch (IOException exec) {
                throw new AcceptClientException(exec.getMessage());
            }

            while (true) {
                try {
                    ObjectInputStream inputStream = new ObjectInputStream(repo.getSockets().get(0).getInputStream());
                    IMessage receivedMessage = (IMessage) inputStream.readObject();
                    TextMessage textMessage = (TextMessage) receivedMessage;

                    System.out.println(textMessage);
                }
                catch (IOException | ClassNotFoundException exec) {
                    //  conn closed
                    throw new ReceiveException(exec.getMessage());
                }
            }
        }
    }

    public void close() {
        executor.shutdown();
        try {
            acceptSocket.close();
        } catch (IOException e) {
            //  conn closed
            e.printStackTrace();
        }
    }
}
