package shared.repo;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class SocketRepo {
    private final List<Socket> sockets = new ArrayList<>();

    public void addSocket(Socket socket) {
        this.sockets.add(socket);
    }

    public List<Socket> getSockets() {
        return this.sockets;
    }

    public void clear() {
        for (Socket socket : sockets) {
            try {
                socket.getInputStream().close();
                socket.getOutputStream().close();
                socket.close();
            } catch (IOException e) {
                //  conn closed
            }
        }
        sockets.clear();
    }
}
