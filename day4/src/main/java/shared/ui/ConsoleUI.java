package shared.ui;

import client.IClientService;
import server.IServerService;
import shared.domain.TextMessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public class ConsoleUI {
    private final String EXIT_MESSAGE = "\\exit";
    private final String CONNECT_MESSAGE = "\\conn";
    private final String HELP_MESSAGE = "\\help";

    public IServerService server;
    public IClientService client;
    private boolean loopUI;

    public ConsoleUI(IServerService server, IClientService client) {
        this.server = server;
        this.client = client;
        this.loopUI = true;
    }

    public void run() {
        showHelp();
        loopUI = true;
        while (loopUI) {
            String message = readLine();
            try {
                parseMessage(message);
            }
            catch (RuntimeException exec) {
                System.out.println("ERROR while doing network operations!");
                System.out.println(exec.getMessage());
                exec.printStackTrace();
            }
        }
    }

    private void parseMessage(String message) {
        if (message.equals(EXIT_MESSAGE)) {
            loopUI = false;
        }
        else if (message.equals(HELP_MESSAGE)) {
            showHelp();
        }
        else if (message.startsWith(CONNECT_MESSAGE)) {
            String[] tokens = message.substring(CONNECT_MESSAGE.length()).strip().split(" ");
            client.connectTo(tokens[0], Integer.parseInt(tokens[1]));
        }
        else {
            sendMessage(message);
        }
    }

    private void sendMessage(String message) {
        Date timestamp = new Date();
        TextMessage toSend = new TextMessage(message, timestamp);
        client.sendTextMessage(toSend);
    }

    private void showHelp() {
        System.out.println("+----------------------------------------------+");
        System.out.println(HELP_MESSAGE + ": show this menu");
        System.out.println(EXIT_MESSAGE + ": gracefully exit");
        System.out.println(CONNECT_MESSAGE + " <ip>: connect to remote PC");
        System.out.println("+----------------------------------------------+");
    }

    private String readLine() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String value = "";
        while (value.isEmpty()) {
            try {
                value = reader.readLine().strip();
            } catch (IOException exec) {
                System.out.println("ERROR reading your keyboard input!");
                System.out.println(exec.getMessage());
            }
        }
        return value;
    }
}
