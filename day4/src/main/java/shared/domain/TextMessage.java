package shared.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TextMessage implements IMessage, Serializable {
    private final Date timestamp;
    private final String content;

    public TextMessage(String content, Date timestamp) {
        this.content = content;
        this.timestamp = timestamp;
    }

    public String getContent() {
        return content;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "["
                + new SimpleDateFormat("hh:mm:ss").format(timestamp)
                + "] "
                + content;
    }
}
