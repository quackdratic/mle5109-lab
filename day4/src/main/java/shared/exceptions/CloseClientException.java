package shared.exceptions;

public class CloseClientException extends RuntimeException {
    public CloseClientException(String msg) {
        super(msg);
    }
}
