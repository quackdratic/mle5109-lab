package shared.exceptions;

public class SendException extends RuntimeException {
    public SendException(String msg) {
        super(msg);
    }
}
