package shared.exceptions;

public class AcceptClientException extends RuntimeException {
    public AcceptClientException(String msg) {
        super(msg);
    }
}
