package shared.exceptions;

public class CloseConnectionException extends RuntimeException {
    public CloseConnectionException(String msg) {
        super(msg);
    }
}
