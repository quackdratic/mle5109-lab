package shared.exceptions;

public class OpenServerException extends RuntimeException {
    public OpenServerException(String msg) {
        super(msg);
    }
}
