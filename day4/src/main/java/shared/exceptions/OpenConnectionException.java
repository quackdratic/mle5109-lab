package shared.exceptions;

public class OpenConnectionException extends RuntimeException {
    public OpenConnectionException(String msg) {
        super(msg);
    }
}
