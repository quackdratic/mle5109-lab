package shared.exceptions;

public class InvalidTargetException extends RuntimeException {
    public InvalidTargetException(String msg) {
        super(msg);
    }
}
