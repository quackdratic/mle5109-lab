package shared.exceptions;

public class ReceiveException extends RuntimeException {
    public ReceiveException(String msg) {
        super(msg);
    }
}
