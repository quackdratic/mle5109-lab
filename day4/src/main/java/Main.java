import client.P2PSender;
import server.P2PReceiver;
import shared.repo.SocketRepo;
import shared.ui.ConsoleUI;

public class Main {
    public static void main(String[] args) {
        SocketRepo repo = new SocketRepo();
        P2PReceiver serverService = new P2PReceiver(repo, Integer.parseInt(args[0]));
        P2PSender clientService = new P2PSender(repo, serverService);

        serverService.openServer();
        clientService.openClient();

        new ConsoleUI(serverService, clientService).run();
        System.out.println("Exit main...");

        clientService.close();
        serverService.close();
        repo.clear();
    }
}
