package client;

import server.P2PReceiver;
import shared.domain.IMessage;
import shared.domain.TextMessage;
import shared.exceptions.InvalidTargetException;
import shared.exceptions.OpenConnectionException;
import shared.exceptions.SendException;
import shared.repo.SocketRepo;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class P2PSender implements IClientService {
    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private final LinkedBlockingQueue<IMessage> messagesToSend = new LinkedBlockingQueue<>();

    private final SocketRepo repo;

    private final P2PReceiver receiver;

    public P2PSender(SocketRepo repo, P2PReceiver receiver) {
        this.repo = repo;
        this.receiver = receiver;
    }

    public void openClient() {
        executor.submit(new Thread(this::pushMessages));
    }

    public void pushMessages() {
        while (true) {
            IMessage toSend = null;
            try {
                toSend = messagesToSend.take();
            } catch (InterruptedException exec) {
                throw new SendException(exec.getMessage());
            }
            IMessage finalToSend = toSend;
            repo.getSockets().forEach(socket -> {
                try {
                    new ObjectOutputStream(socket.getOutputStream()).writeObject(finalToSend);
                } catch (IOException exec) {
                    //  conn closed
                    throw new SendException(exec.getMessage());
                }
            });
        }
    }

    public void sendTextMessage(TextMessage toSend) {
        messagesToSend.offer(toSend);
    }

    public void connectTo(String connectIP, int peerPort) {
        try {
            Socket socket = new Socket(connectIP, peerPort);
            repo.addSocket(socket);
            System.out.println("LOG: successfully connected to <" + connectIP + ">!");
        }
        catch (UnknownHostException exec) {
            throw new InvalidTargetException(exec.getMessage());
        }
        catch (IOException exec) {
            throw new OpenConnectionException(exec.getMessage());
        }
    }

    public void close() {
        executor.shutdownNow();
    }
}
