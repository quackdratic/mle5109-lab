package client;

import shared.domain.TextMessage;

public interface IClientService {
    void connectTo(String connectIP, int port);
    void sendTextMessage(TextMessage toSend);
    void openClient();
    void close();
}
